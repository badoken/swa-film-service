/** Unites modules in runnable application. */
module com.badoken.swa.film.boot {
    requires com.badoken.swa.film.data;
    requires com.badoken.swa.film.common;
    requires com.badoken.swa.film.core;
    requires com.badoken.swa.film.web;

    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;
    requires lombok;
    requires org.mapstruct.processor;

    exports com.badoken.swa.film.boot;
}