/** Web layer contains controllers. */
module com.badoken.swa.film.web {
    requires com.badoken.swa.film.common;
    requires com.badoken.swa.film.core;

    requires spring.web;
    requires spring.beans;
    requires spring.context;
    requires lombok;

    exports com.badoken.swa.film.web.conf;
}