package com.badoken.swa.film.core.api;

import com.badoken.swa.film.common.model.Film;

import java.util.Optional;

public interface FilmManager {
    Optional<Film> getFilm(long id);
}
