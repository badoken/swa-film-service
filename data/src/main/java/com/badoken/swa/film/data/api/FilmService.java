package com.badoken.swa.film.data.api;

import com.badoken.swa.film.common.model.Film;

import java.util.Optional;

public interface FilmService {
    Optional<Film> find(long id);
}
