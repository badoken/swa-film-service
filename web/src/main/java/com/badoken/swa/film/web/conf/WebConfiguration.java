package com.badoken.swa.film.web.conf;

import com.badoken.swa.film.core.api.FilmManager;
import com.badoken.swa.film.web.controller.MainController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


public @Configuration class WebConfiguration {
    private @Autowired FilmManager filmManager;
    public @Bean MainController mainController() { return new MainController(filmManager); }
}
