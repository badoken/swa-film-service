package com.badoken.swa.film.data.conf;

import com.badoken.swa.film.data.api.FilmService;
import com.badoken.swa.film.data.impl.FilmDataService;
import lombok.extern.slf4j.Slf4j;
import org.postgresql.Driver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;

import static java.lang.Boolean.FALSE;

@Slf4j
@PropertySource("classpath:db.properties")
public @Configuration class DataConfiguration {
    private static final Properties ADDITIONAL_PROPERTIES = new Properties();

    static {
        ADDITIONAL_PROPERTIES.putAll(Map.of(
                "hibernate.temp.use_jdbc_metadata_defaults", FALSE,
                "hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect"
        ));
    }

    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    public @Bean FilmService filmService() { return new FilmDataService(); }

    public @Bean DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Driver.class.getName());
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        log.debug("Created datasource: url = {}; u/p = {} / {}", dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());

        return dataSource;
    }

    public @Bean LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean
                = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setPackagesToScan("com.badoken.swa.film.data.impl.entity");

        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(ADDITIONAL_PROPERTIES);

        return factoryBean;
    }

    public @Bean PlatformTransactionManager transactionManager(EntityManagerFactory factory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(factory);

        return transactionManager;
    }

    public @Bean PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
