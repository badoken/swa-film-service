/** Service layer contains main xbusiness logic. */
module com.badoken.swa.film.core {
    requires com.badoken.swa.film.common;
    requires com.badoken.swa.film.data;

    requires spring.context;
    requires spring.beans;
    requires lombok;

    exports com.badoken.swa.film.core.api;
    exports com.badoken.swa.film.core.conf;
}