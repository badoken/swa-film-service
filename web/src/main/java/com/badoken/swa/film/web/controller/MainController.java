package com.badoken.swa.film.web.controller;

import com.badoken.swa.film.common.model.Film;
import com.badoken.swa.film.core.api.FilmManager;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RequestMapping("film")
@RequiredArgsConstructor
public @RestController class MainController {
    private final @NonNull FilmManager filmManager;

    @GetMapping("get")
    public Film getFilm(@RequestParam("id") Long id) {
        return Optional.ofNullable(id)
                .filter(i -> i > 0)
                .flatMap(filmManager::getFilm)
                .orElse(null);
    }
}
