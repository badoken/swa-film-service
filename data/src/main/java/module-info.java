/** Data layer module contains DB and other persistent stuff. */
module com.badoken.swa.film.data {
    requires com.badoken.swa.film.common;

    requires hibernate.entitymanager;
    requires org.hibernate.orm.core;
    requires postgresql;
    requires spring.jdbc;
    requires java.sql;
    requires spring.orm;
    requires spring.data.jpa;

    requires spring.context;
    requires spring.beans;
    requires spring.tx;

    requires slf4j.api;

    requires lombok;
    requires jsr305;
    requires java.persistence;

    exports com.badoken.swa.film.data.api;
    exports com.badoken.swa.film.data.conf;
}