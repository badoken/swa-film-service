module com.badoken.swa.film.common {
    requires lombok;
    requires jsr305;

    exports com.badoken.swa.film.common.model;
}