package com.badoken.swa.film.data.impl;

import com.badoken.swa.film.common.model.Film;
import com.badoken.swa.film.data.api.FilmService;
import com.badoken.swa.film.data.impl.entity.FilmData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

public @Service class FilmDataService implements FilmService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public Optional<Film> find(long id) {
        return Optional.ofNullable(entityManager.find(FilmData.class, id))
                .map(FilmData::toFilm);
    }
}
