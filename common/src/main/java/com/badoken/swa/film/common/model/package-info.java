/** Common model shared in application. */
@javax.annotation.ParametersAreNonnullByDefault
package com.badoken.swa.film.common.model;