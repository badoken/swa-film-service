#! /bin/bash

build_folder=/tmp/swa/docker

mkdir -p ${build_folder}/scripts
cp ../initial/* ${build_folder}/scripts
cp ./Dockerfile  ${build_folder}
cd ${build_folder}

docker build -t swt:db .
app_docker_pid=`docker run -p 127.0.0.1:5432:6543/tcp -d swt:db`

rm -rf build_folder/*

echo "Running docker PID = ${app_docker_pid}"