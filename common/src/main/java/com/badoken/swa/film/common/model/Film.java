package com.badoken.swa.film.common.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

import static java.util.Objects.requireNonNull;
import static lombok.AccessLevel.PRIVATE;

@Getter
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor(access = PRIVATE)
@ToString
@EqualsAndHashCode
public final class Film {
    long id;
    @NonNull String name;
    @NonNull LocalDate created;

    public static Film newInstance(long id, String name, LocalDate created) {
        if (id < 0) throw new IllegalArgumentException("Negative id");
        if (name.isBlank()) throw new IllegalArgumentException("Blank name");
        return new Film(id, requireNonNull(name), requireNonNull(created));
    }
}
