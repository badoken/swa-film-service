package com.badoken.swa.film.data.impl.entity;

import com.badoken.swa.film.common.model.Film;
import org.junit.Test;

import static java.time.LocalDate.now;
import static org.junit.Assert.*;

public class FilmDataTest {
    @Test
    public void testValueOfCreatesSimilarData() {
        // given
        var film = Film.newInstance(1, "name", now());

        // when
        var result = FilmData.valueOf(film);

        // then
        assertNotNull(result);
        assertEquals(film.getId(), result.getId());
        assertEquals(film.getName(), result.getName());
        assertEquals(film.getCreated(), result.getCreated());
    }

    @Test
    public void testToFilmReturnsSimilarFilm() {
        // given
        var filmData = new FilmData(1, "name", now());

        // when
        var result = filmData.toFilm();

        // then
        assertNotNull(result);
        assertEquals(filmData.getId(), result.getId());
        assertEquals(filmData.getName(), result.getName());
        assertEquals(filmData.getCreated(), result.getCreated());
    }
}