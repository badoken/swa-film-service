package com.badoken.swa.film.data.impl.entity;

import com.badoken.swa.film.common.model.Film;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

import static lombok.AccessLevel.PUBLIC;

@Table(name = "film")
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor(access = PUBLIC)
@DynamicUpdate
@Entity
public final class FilmData {
    private @Id long id;
    private @Column String name;
    private @Column LocalDate created;

    public static FilmData valueOf(Film film) { return new FilmData(film.getId(), film.getName(), film.getCreated()); }

    public Film toFilm() { return Film.newInstance(id, name, created); }
}