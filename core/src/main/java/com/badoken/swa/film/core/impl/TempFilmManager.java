package com.badoken.swa.film.core.impl;

import com.badoken.swa.film.common.model.Film;
import com.badoken.swa.film.core.api.FilmManager;
import com.badoken.swa.film.data.api.FilmService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class TempFilmManager implements FilmManager {
    private final @NonNull FilmService filmService;

    @Override
    public Optional<Film> getFilm(long id) { return filmService.find(id); }
}
