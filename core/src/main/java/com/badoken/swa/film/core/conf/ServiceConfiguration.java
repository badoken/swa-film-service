package com.badoken.swa.film.core.conf;

import com.badoken.swa.film.core.api.FilmManager;
import com.badoken.swa.film.data.api.FilmService;
import com.badoken.swa.film.core.impl.TempFilmManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public @Configuration class ServiceConfiguration {
    private @Autowired FilmService filmService;

    public @Bean FilmManager textManager() { return new TempFilmManager(filmService); }
}
