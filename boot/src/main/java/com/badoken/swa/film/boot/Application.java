package com.badoken.swa.film.boot;

import com.badoken.swa.film.core.conf.ServiceConfiguration;
import com.badoken.swa.film.data.conf.DataConfiguration;
import com.badoken.swa.film.web.conf.WebConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@Import({
                DataConfiguration.class,
                ServiceConfiguration.class,
                WebConfiguration.class
        })
public class Application {
    public static void main(String[] args) { run(Application.class, args); }
}
